import * as React from 'react'
import { Button } from 'antd'
import RolesWindow from '../blocks/roles-window'
import RadioRole from '../components/role-radio'

const Index = () => {
  return (
    <div>
      <RolesWindow />
    </div>
  )
}
export default Index
