// @flow
import * as React from 'react'
import Radio from 'antd/lib/radio/radio'
import { RoleRadioContainer } from './styled'
import Image from 'next/image'
import Paragraph from 'antd/lib/typography/Paragraph'

const RadioRole = ({ image, label }) => {
  return (
    <>
      <RoleRadioContainer value={1}>
        <Image src={image} width={46} height={56} />
        <Paragraph>{label}</Paragraph>
      </RoleRadioContainer>
    </>
  )
}
export default RadioRole
