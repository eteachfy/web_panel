import styled from '@emotion/styled'
import { Radio } from 'antd'

export const RoleRadioContainer = styled(Radio)`
  width: 192px;
  height: 145px;
  border-radius: 4px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  &:hover {
    border: 1px solid #1a595a;
  }
  .ant-radio {
    margin-left: -50%;
  }
`
