import { Client, Agent } from '../blocks'

export function preftifyObj(user, role): UserType {
  const _user: UserType = {
    profile: {
      sub: user.claims.sub,
      email: user.claims.email,
      name: user.claims.name,
      picture: user.claims.picture,
      lastLogin: user.authTime,
    },
  }
  const action = user.claims.isAgent
    ? 'isAgent'
    : user.claims.isClient
    ? 'isClient'
    : null
  switch (action) {
    case 'isAgent':
      return { ..._user, isAgent: user.claims.isAgent }
    case 'isClient':
      return { ..._user, isClient: user.claims.isClient }
    default:
      return { ..._user, [role]: true }
  }
}

export const checkCompenent = (type) => {
  if (!type) return
  const action = type.isAgent ? 'isAgent' : type.isClient ? 'isClient' : null
  return { isAgent: Agent, isClient: Client }[action]
}
