import styled from '@emotion/styled'

export const LeftRoleWindow = styled.div`
  width: 600px;
  height: 100vh;
  margin: 0 250px 0 0;
  padding: 24px 101px 255px;
  background-color: #ffffff;
`
export const LeftInnerBox = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  padding: 24px;
  max-width: 398px;
  img {
    margin-bottom: 200px;
    max-width: 398px;
  }
`
export const RightRoleWindow = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const RightInnerBox = styled.div`
  width: 500px;
  height: 510px;
  margin: 285px 0 285px 250px;
  padding: 46px 50px 55px;
  border-radius: 8px;
  box-shadow: 0 12px 30px 0 rgba(19, 21, 35, 0.1);
  background-color: #ffffff;
`
export const RolesWindowContainer = styled.div`
  height: 100vh;
`
