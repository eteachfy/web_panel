// @flow
import * as React from 'react'
import { Row, Col, Button } from 'antd'
import {
  LeftRoleWindow,
  RightRoleWindow,
  RolesWindowContainer,
  LeftInnerBox,
  RightInnerBox,
} from './styled'
import Image from 'next/image'
import Paragraph from 'antd/lib/typography/Paragraph'
import Title from 'antd/lib/typography/Title'
import RadioRole from '../../components/role-radio'

const RolesWindow = () => {
  return (
    <RolesWindowContainer>
      <Row>
        <Col xs={24} md={8}>
          <LeftRoleWindow>
            <Image src="/logo.svg" width={193} height={54} />
            <LeftInnerBox>
              <Image src="/login-books.svg" width={355} height={280} />
              <Title level={4}>Welcome To e-Teachfy</Title>
              <Paragraph>
                Donec facilisis tortor ut augue lacinia, at viverra est semper.
                Sed sapien metus, scelerisque nec pharetra id, tempor a tortor.
                Pellentesque non dignissim neque.
              </Paragraph>
            </LeftInnerBox>
          </LeftRoleWindow>
        </Col>
        <Col xs={24} md={16}>
          <RightRoleWindow>
            <RightInnerBox>
              <Title level={2}>Sign in</Title>
              <Paragraph>Please select user type ...</Paragraph>
              <Row gutter={[24, 24]}>
                <Col md={12}>
                  <RadioRole image="/family.svg" label="Parent" />
                </Col>
                <Col md={12}>
                  <RadioRole image="/study.svg" label="Teacher" />
                </Col>
                <Col md={12}>
                  <RadioRole image="/reading.svg" label="Student" />
                </Col>
                <Col md={12}>
                  <RadioRole image="/admin-with-cogwheels.svg" label="Admin" />
                </Col>
              </Row>
            </RightInnerBox>
          </RightRoleWindow>
        </Col>
      </Row>
    </RolesWindowContainer>
  )
}
export default RolesWindow
