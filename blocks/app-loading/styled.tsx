import styled from '@emotion/styled'

export const AppLoadingContianer = styled.div`
  position: relative;
  flex-direction: column;
  z-index: 2;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`
