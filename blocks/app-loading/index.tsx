import React, { memo } from 'react'
import { Spin, Typography } from 'antd'
import { AppLoadingContianer } from './styled'

const { Title } = Typography
const AppLoading = ({ title }: any) => {
  return (
    <AppLoadingContianer>
      <Spin size="large" />
      {title && (
        <Title title="STUDENT PORTAL" level={2}>
          {title}
        </Title>
      )}
    </AppLoadingContianer>
  )
}
export default memo(AppLoading)
